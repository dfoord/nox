/// <reference path='../node_modules/@types/mocha/index.d.ts' />

import assert = require('assert');
import fs = require('fs');

import { IEncryptor, Encryptor } from '../src/encryptor';
import { IDecryptor, Decryptor } from '../src/decryptor';

const infile: string = './test/test.txt';
const outfile: string = './test/test.txt.exo';
const password: string = 'password';

let original: string = fs.readFileSync(infile).toString();

describe('nox', () => {

  let decryptor: IDecryptor = new Decryptor();
  let encryptor: IEncryptor = new Encryptor();

  describe.only('Encryptor.encrypt()', () => {

    it('should create an encrypted file', async () => {
      await encryptor.encrypt(infile, outfile, password);

      assert.equal(true, fs.existsSync(outfile));

      let plain: string = fs.readFileSync(infile).toString();
      let encrypted: string = fs.readFileSync(outfile).toString();

      assert.equal(original, plain);
      assert.notEqual(plain, encrypted);
    });

  });

  describe.only('Decryptor.decrypt()', () => {

    it('should decrypt an encrypted file', async () => {
      await decryptor.decrypt(outfile, infile, password);

      let encrypted: string = fs.readFileSync(outfile).toString();
      let decrypted: string = fs.readFileSync(infile).toString();

      assert.equal(original, decrypted);
    });

    it('should throw an error when an incorrect password is supplied', async () => {
      try {
        await decryptor.decrypt(outfile, infile, 'not the password');
      } catch (e) {
        assert.equal('bad decrypt', e.message);
        assert.equal(false, fs.existsSync(`${outfile}.tmp`));
      }
    });

  });

});