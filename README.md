# nox
A simple file encryption tool.

### Installation

To use nox, NodeJS needs to be installed (min version v6.10.2).

After NodeJS is installed enter `npm run installer` as an admininstrator.

### Commands

`nox [command]`

Commands:

 - Encrypt a file: `nox encrypt [options] <infile> `
 - Decrypt a file: `nox decrypt [options] <infile>`

#### Encrypt
Usage: `encrypt [options] <infile>`

Options:

    -h, --help            output usage information
    -p, --password <pwd>  Password to encrypt file with

#### Decrypt
Usage: `decrypt [options] <infile>`

Options:

    -h, --help            output usage information
    -p, --password <pwd>  Password to decrypt file with
