#!/usr/bin/env node

import program = require('commander');
import ora = require('ora');
import { version } from '../package.json';

import { Logger } from './logger';
import { IEncryptor, Encryptor } from './encryptor';
import { IDecryptor, Decryptor } from './decryptor';


const spinner = ora({
  spinner: 'dots'
});

program
  .version(version)

/**
 * Encrypt command
 */
program
  .command('encrypt <infile>')
  .description('Encrypt a file')
  .option('-p, --password <pwd>', 'Password to encrypt file with')
  .action(async (infile, options) => {

    const encryptor: IEncryptor = new Encryptor();
    const outfile = `${infile}.exo`;

    spinner.start();

    try {

      await encryptor.encrypt(infile, outfile, options.password, (percent: number) => {
        spinner.text = `Encrypting ${infile} to ${outfile}... ${percent}%`;
      });

      spinner.succeed(`Encrypting ${infile} to ${outfile}... COMPLETE`);
      process.exit(0);
    }
    catch (e) {
      Logger.error(e);
      process.exit(1);
    }

  });

/**
 * Decrypt command
 */

function handleDecryptError(err, props) {
  switch (err.message) {
    // Incorrect password
    case 'bad decrypt':
      // Tell the user what went wrong
      spinner.fail(`Decrypting ${props.infile} to ${props.outfile}... FAILED: Incorrect password`);
      break;
    default:
      Logger.error(err);
      process.exit(1);
  }
}

program
  .command('decrypt <infile>')
  .description('Decrypt a file')
  .option('-p, --password <pwd>', 'Password to decrypt file with')
  .action(async (infile, options) => {

    const decryptor: IDecryptor = new Decryptor();
    // The output on successful decrypt
    const outfile = `${infile.substring(0, infile.length - 4)}`;

    spinner.start();

    try {

      // Wait for the file to decrypt to the temp location
      await decryptor.decrypt(infile, outfile, options.password, (percent: number) => {
        spinner.text = `Decrypting ${infile} to ${outfile}... ${percent}%`;
      });

      // If we got here, then the decrypt was successful
      spinner.succeed(`Decrypting ${infile} to ${outfile}... COMPLETE`);

      process.exit(0);
    }
    catch (e) {
      handleDecryptError(e, {
        infile,
        outfile
      });
    }
  });

program.parse(process.argv);