"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var crypto = require("crypto");
var command_base_1 = require("./command-base");
var stream_progress_1 = require("./stream-progress");
/**
 * File encryptor implementation
 */
var Encryptor = /** @class */ (function (_super) {
    __extends(Encryptor, _super);
    function Encryptor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Encrypt a file
     * @param infile - The path of the file to encrypt
     * @param password - The password to encrypt the file with
     * @param onProgress - Callback that emits copy percentage
     */
    Encryptor.prototype.encrypt = function (infile, outfile, password, onProgress) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                _this.verifyFileExists(infile);
                _this.verifyPasswordSupplied(password);
                var hash = crypto.createHash('sha256')
                    .update(password)
                    .digest('base64');
                var cipher = crypto.createCipher('aes256', hash);
                var input = fs.createReadStream(infile);
                var output = fs.createWriteStream(outfile);
                var fileStats = fs.statSync(infile);
                if (onProgress) {
                    stream_progress_1.StreamProgressTracker.trackProgress(input, fileStats.size, onProgress);
                }
                output.on('close', function () {
                    resolve();
                });
                input
                    .pipe(cipher)
                    .pipe(output);
            }
            catch (e) {
                reject(e);
            }
        });
    };
    return Encryptor;
}(command_base_1.CommandBase));
exports.Encryptor = Encryptor;
