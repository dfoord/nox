import fs = require('fs');
import crypto = require('crypto');
import { CommandBase } from './command-base';
import { StreamProgressTracker } from './stream-progress';

/**
 * Represents a file encryptor
 */
export interface IEncryptor {

  /**
   * Encrypt a file
   * @param infile - The path of the file to encrypt
   * @param password - The password to encrypt the file with
   * @param onProgress - Callback that emits copy percentage
   */
  encrypt(infile: string,
    outfile: string,
    password: string,
    onProgress?: (percentComplete: number) => void): Promise<any>;
}

/**
 * File encryptor implementation
 */
export class Encryptor extends CommandBase implements IEncryptor {

  /**
   * Encrypt a file
   * @param infile - The path of the file to encrypt
   * @param password - The password to encrypt the file with
   * @param onProgress - Callback that emits copy percentage
   */
  encrypt(infile: string,
    outfile: string,
    password: string,
    onProgress?: (percentComplete: number) => void): Promise<any> {

    return new Promise((resolve, reject) => {
      try {

        this.verifyFileExists(infile);
        this.verifyPasswordSupplied(password);

        const hash = crypto.createHash('sha256')
                   .update(password)
                   .digest('base64');

        let cipher: crypto.Cipher = crypto.createCipher('aes256', hash);
        const input: fs.ReadStream = fs.createReadStream(infile);
        const output: fs.WriteStream = fs.createWriteStream(outfile);

        let fileStats: fs.Stats = fs.statSync(infile);

        if (onProgress) {
          StreamProgressTracker.trackProgress(input, fileStats.size, onProgress);
        }

        output.on('close', () => {
          resolve();
        });

        input
          .pipe(cipher)
          .pipe(output);

      } catch (e) {
        reject(e);
      }
    });
  }

}