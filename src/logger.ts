import readline = require('readline');

export class Logger {

  private static out: NodeJS.WritableStream = process.stdout;

  static single(message: string): void {
    readline.clearLine(Logger.out, 1);
    Logger.out.write('\r');
    Logger.out.write(message);
  }

  static writeLine(message: string): void {
    Logger.out.write(`${message}\n`);
  }

  static write(message: string): void {
    Logger.out.write(message);
  }

  static error(err: Error): void {
    console.error(err);
  }

}
