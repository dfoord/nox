import fs = require('fs');

export class StreamProgressTracker {

  static trackProgress(stream: fs.ReadStream,
    totalBytes: number,
    onTick: (percentComplete: number) => void): void {

    let percentComplete = 0;
    let copiedBytes: number = 0;

    stream.on('data', (chunk) => {
      copiedBytes += chunk.length;
      let currentPercentage: number = Math.floor((copiedBytes / totalBytes) * 100);
      if (currentPercentage !== percentComplete) {
        percentComplete = currentPercentage;
        onTick.call(null, percentComplete);
      }
    });
  }

}