"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var CommandBase = /** @class */ (function () {
    function CommandBase() {
    }
    CommandBase.prototype.verifyFileExists = function (filePath) {
        var exists = fs.existsSync(filePath);
        if (!exists) {
            console.error("error: file '" + filePath + "' does not exist.");
            process.exit(1);
        }
    };
    CommandBase.prototype.verifyPasswordSupplied = function (password) {
        if (!password) {
            console.error('error: option "-p, --password <pwd>" argument missing');
            process.exit(1);
        }
    };
    return CommandBase;
}());
exports.CommandBase = CommandBase;
