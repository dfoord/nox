#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var program = require("commander");
var ora = require("ora");
var package_json_1 = require("../package.json");
var logger_1 = require("./logger");
var encryptor_1 = require("./encryptor");
var decryptor_1 = require("./decryptor");
var spinner = ora({
    spinner: 'dots'
});
program
    .version(package_json_1.version);
/**
 * Encrypt command
 */
program
    .command('encrypt <infile>')
    .description('Encrypt a file')
    .option('-p, --password <pwd>', 'Password to encrypt file with')
    .action(function (infile, options) { return __awaiter(_this, void 0, void 0, function () {
    var encryptor, outfile, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                encryptor = new encryptor_1.Encryptor();
                outfile = infile + ".exo";
                spinner.start();
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, encryptor.encrypt(infile, outfile, options.password, function (percent) {
                        spinner.text = "Encrypting " + infile + " to " + outfile + "... " + percent + "%";
                    })];
            case 2:
                _a.sent();
                spinner.succeed("Encrypting " + infile + " to " + outfile + "... COMPLETE");
                process.exit(0);
                return [3 /*break*/, 4];
            case 3:
                e_1 = _a.sent();
                logger_1.Logger.error(e_1);
                process.exit(1);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
/**
 * Decrypt command
 */
function handleDecryptError(err, props) {
    switch (err.message) {
        // Incorrect password
        case 'bad decrypt':
            // Tell the user what went wrong
            spinner.fail("Decrypting " + props.infile + " to " + props.outfile + "... FAILED: Incorrect password");
            break;
        default:
            logger_1.Logger.error(err);
            process.exit(1);
    }
}
program
    .command('decrypt <infile>')
    .description('Decrypt a file')
    .option('-p, --password <pwd>', 'Password to decrypt file with')
    .action(function (infile, options) { return __awaiter(_this, void 0, void 0, function () {
    var decryptor, outfile, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                decryptor = new decryptor_1.Decryptor();
                outfile = "" + infile.substring(0, infile.length - 4);
                spinner.start();
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                // Wait for the file to decrypt to the temp location
                return [4 /*yield*/, decryptor.decrypt(infile, outfile, options.password, function (percent) {
                        spinner.text = "Decrypting " + infile + " to " + outfile + "... " + percent + "%";
                    })];
            case 2:
                // Wait for the file to decrypt to the temp location
                _a.sent();
                // If we got here, then the decrypt was successful
                spinner.succeed("Decrypting " + infile + " to " + outfile + "... COMPLETE");
                process.exit(0);
                return [3 /*break*/, 4];
            case 3:
                e_2 = _a.sent();
                handleDecryptError(e_2, {
                    infile: infile,
                    outfile: outfile
                });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
program.parse(process.argv);
