import fs = require('fs');
import crypto = require('crypto');
import { CommandBase } from './command-base';
import { StreamProgressTracker } from './stream-progress';
import { FileSystem } from './file-system';

/**
 * Represents a file decryptor
 */
export interface IDecryptor {

  /**
   * Decrypt a file
   * @param infile - The path of the file to decrypt
   * @param password - The password to decrypt the file with
   * @param progress - Callback that emits copy percentage
   */
  decrypt(infile: string,
    outfile: string,
    password: string,
    progress?: (percentComplete: number) => void): Promise<any>;
}

/**
 * File decryptor implementation
 */
export class Decryptor extends CommandBase implements IDecryptor {

  /**
  * Decrypt a file
  * @param infile - The path of the file to decrypt
  * @param password - The password to decrypt the file with
  * @param progress - Callback that emits copy percentage
  */
  decrypt(infile: string,
    outfile: string,
    password: string,
    progress?: (percentComplete: number) => void): Promise<any> {

    return new Promise((resolve, reject) => {
      try {

        this.verifyFileExists(infile);
        this.verifyPasswordSupplied(password);

        const hash = crypto.createHash('sha256')
                   .update(password)
                   .digest('base64');

        const decipher: crypto.Decipher = crypto.createDecipher('aes256', hash);
        const input: fs.ReadStream = fs.createReadStream(infile);

        const temp = `${outfile}.tmp`
        const output: fs.WriteStream = fs.createWriteStream(temp);

        const fileStats: fs.Stats = fs.statSync(infile);

        if (progress) {
          StreamProgressTracker.trackProgress(input, fileStats.size, progress);
        }

        decipher.on('error', async (e) => {
          await FileSystem.unlinkAsync(temp);
          if (e.message.indexOf('bad decrypt') > -1) {
            reject(new Error('bad decrypt'));
          }
          reject(e);
        });

        output.on('close', async () => {
          // Renamed the temp file to the output
          await FileSystem.renameAsync(temp, outfile);
          resolve();
        });

        input
          .pipe(decipher)
          .pipe(output);

      } catch (e) {

        reject(e);
      }
    });
  }

}