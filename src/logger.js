"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var readline = require("readline");
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.single = function (message) {
        readline.clearLine(Logger.out, 1);
        Logger.out.write('\r');
        Logger.out.write(message);
    };
    Logger.writeLine = function (message) {
        Logger.out.write(message + "\n");
    };
    Logger.write = function (message) {
        Logger.out.write(message);
    };
    Logger.error = function (err) {
        console.error(err);
    };
    Logger.out = process.stdout;
    return Logger;
}());
exports.Logger = Logger;
