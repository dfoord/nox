import fs = require('fs');

export class FileSystem {

  static renameAsync(oldPath: string, newPath: string): Promise<any> {
    return new Promise((resolve, reject) => {
      fs.rename(oldPath, newPath, (err) => {
        if (err) {
          reject(err);
          return;
        }
        resolve();
      });
    });
  }

  static unlinkAsync(path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      fs.unlink(path, (err) => {
        if (err) {
          reject(err);
          return;
        }
        resolve();
      });
    });
  }

}