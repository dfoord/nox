"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var FileSystem = /** @class */ (function () {
    function FileSystem() {
    }
    FileSystem.renameAsync = function (oldPath, newPath) {
        return new Promise(function (resolve, reject) {
            fs.rename(oldPath, newPath, function (err) {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    };
    FileSystem.unlinkAsync = function (path) {
        return new Promise(function (resolve, reject) {
            fs.unlink(path, function (err) {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    };
    return FileSystem;
}());
exports.FileSystem = FileSystem;
