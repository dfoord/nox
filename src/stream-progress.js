"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StreamProgressTracker = /** @class */ (function () {
    function StreamProgressTracker() {
    }
    StreamProgressTracker.trackProgress = function (stream, totalBytes, onTick) {
        var percentComplete = 0;
        var copiedBytes = 0;
        stream.on('data', function (chunk) {
            copiedBytes += chunk.length;
            var currentPercentage = Math.floor((copiedBytes / totalBytes) * 100);
            if (currentPercentage !== percentComplete) {
                percentComplete = currentPercentage;
                onTick.call(null, percentComplete);
            }
        });
    };
    return StreamProgressTracker;
}());
exports.StreamProgressTracker = StreamProgressTracker;
