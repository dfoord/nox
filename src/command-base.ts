import fs = require('fs');

export abstract class CommandBase {

  verifyFileExists(filePath: string): void {
    let exists: boolean = fs.existsSync(filePath);
    if (!exists) {
      console.error(`error: file '${filePath}' does not exist.`);
      process.exit(1);
    }
  }

  verifyPasswordSupplied(password: string): void {
    if (!password) {
      console.error('error: option "-p, --password <pwd>" argument missing');
      process.exit(1);
    }
  }

}